import cv2
import fitter
import matplotlib.axes
import matplotlib.figure
import numpy as np
import os
from matplotlib import pyplot as plt
from uncertainties import unumpy

GRAPHS_FOLDER = '../Graphs'


def RLC_fit_func(t, I_max, omega, phi, tau):
    return I_max * np.sin(omega * t + phi) * np.exp(-t / tau)


def short_circuit():
    data = np.loadtxt('../Data/SC_shal_I.csv', usecols=[3, 4], delimiter=',')
    t = data[:, 0]
    I = data[:, 1]

    start_t = 5.18e-7
    start_t_index = np.where(t > start_t)[0][0]

    t = t[start_t_index:]
    I = I[start_t_index:]

    t = t - start_t

    params = fitter.FitParams(RLC_fit_func, t, I)
    params.bounds = ((0, 0.5e6, -np.pi, 0), (100, 4e6, 2 * np.pi, 1e-4))
    fit = fitter.FuncFit(params)

    fig, ax = plt.subplots()
    assert isinstance(fig, matplotlib.figure.Figure)
    assert isinstance(ax, matplotlib.axes.Axes)
    fig.suptitle('RLC Circuit')
    ax.set_title('Current vs time')
    ax.set_xlabel('t [s]')
    # TODO: Is it really the current???
    ax.set_ylabel('Current [A]')

    fit.plot_data(ax)
    fit.plot_fit(ax)

    fig.savefig(os.path.join(GRAPHS_FOLDER, 'RLC-current-vs-t.png'))

    fit.print_results()

    print('---------------------')

    C = 10.6e-6

    I_max, omega, phi, tau = fit.fit_results
    L = 1 / (C * (omega ** 2 + (1 / tau) ** 2))

    V_ch = 27e3
    expected_I_max = V_ch * unumpy.sqrt(C / L.nominal_value)

    print(f'L={L}')
    print(f'expected I max={expected_I_max}')
    print(f'Ratio={expected_I_max / I_max}')
    att = 117
    print(f'RCC={expected_I_max / I_max / att}')


def make_smooth(array, smooth_factor):
    averaged = cv2.resize(array, (1, int(len(array) / smooth_factor)), interpolation=cv2.INTER_AREA)
    return cv2.resize(averaged, (1, len(array)), interpolation=cv2.INTER_CUBIC).flatten()



def single_wire_600():
    I_data = np.loadtxt('../Data/RW_Cu_600_I.csv', usecols=[3, 4], delimiter=',')
    V_data = np.loadtxt('../Data/RW_Cu_600_V.csv', usecols=[3, 4], delimiter=',')
    t = I_data[:, 0]
    I = I_data[:, 1]
    V = V_data[:, 1]

    start_t = 3.32e-7 + -200e-8
    # start_t = 3.32e-7 + 2.68e-8
    # start_t = 3.32e-7 + 2.68e-8 + 8e-8
    # start_t = 2.68e-8
    start_t_index = np.where(t > start_t)[0][0]

    t = t[start_t_index:]
    I = I[start_t_index:]
    V = V[start_t_index:]

    V *= 1000

    t = t - start_t

    ratio = 4.426e4
    I = I * ratio

    smooth_factor = 40
    original_I = I
    I = make_smooth(I, smooth_factor)

    dt = t[1] - t[0]

    dIdt = np.diff(I) / dt
    original_dIdt = dIdt
    dIdt = make_smooth(dIdt, 100)

    t = t[:-1]
    V = V[:-1]
    I = I[:-1]
    original_I = original_I[:-1]
    # L = 0.06e-9
    print(f'dt={dt}')
    # L = 38e-14
    L = 50e-9
    V_R = V - L * dIdt

    fig, axes = plt.subplots(2, 2)

    axes = axes.flat

    axes[0].plot(t, original_I, '.')
    axes[0].set_title('original I vs t')

    axes[1].plot(t, I, '.')
    axes[1].set_title('smoothed I vs t')

    axes[2].plot(t, original_dIdt, '.')
    axes[2].set_title('original dI/dt vs t')

    axes[3].plot(t, dIdt, '.')
    axes[3].set_title('smoothed dI/dt vs t')

    fig, axes = plt.subplots(1, 2)
    ax = axes[0]
    ax.plot(t, V, '.')
    ax.set_title('V vs t')
    ax = axes[1]
    ax.plot(t, V_R, '.')
    ax.set_title('V_R vs t')

    power = V_R * I
    E_vs_t = np.cumsum(power) * dt
    fig, axes = plt.subplots(2)
    ax: matplotlib.axes.Axes = axes[0]
    assert isinstance(fig, matplotlib.figure.Figure)
    ax.plot(t, power)
    ax.set_title('Power on wire vs time')
    ax.label_outer()
    ax.set_ylabel('Power [W]')

    ax: matplotlib.axes.Axes = axes[1]
    ax.plot(t, E_vs_t)
    ax.set_title('Energy vs time')
    ax.set_xlabel('time [s]')
    ax.set_ylabel('Energy [J]')

    fig.savefig(os.path.join(GRAPHS_FOLDER, 'single-wire-0.6mm.png'))

    E = np.sum(power) * dt
    print(f'energy={E}')

    C = 10.6e-6
    V_in = 27e3
    E_in = 1 / 2 * C * V_in ** 2
    print(f'E in={E_in}')
    print(f'efficiency={E / E_in}')



def single_wire_100():
    I_data = np.loadtxt('../Data/RW_Cu_100_I.csv', usecols=[3, 4], delimiter=',')
    V_data = np.loadtxt('../Data/RW_Cu_100_V.csv', usecols=[3, 4], delimiter=',')
    t = I_data[:, 0]
    I = I_data[:, 1]
    V = V_data[:, 1]

    # start_t = t[0]+2.21e-6+1.24e-6
    start_t = t[0]+2.21e-6
    start_t_index = np.where(t > start_t)[0][0]

    t = t[start_t_index:]
    I = I[start_t_index:]
    V = V[start_t_index:]

    V *= 1000

    t = t - start_t

    ratio = 4.426e4
    I = I * ratio

    smooth_factor = 40
    original_I = I
    I = make_smooth(I, smooth_factor)

    dt = t[1] - t[0]

    dIdt = np.diff(I) / dt
    original_dIdt = dIdt
    dIdt = make_smooth(dIdt, 100)

    t = t[:-1]
    V = V[:-1]
    I = I[:-1]
    original_I = original_I[:-1]
    # print(f'dt={dt}')
    # L = 0.06e-9
    # L = 38e-14
    L = 70e-9
    V_R = V - L * dIdt

    fig, axes = plt.subplots(2, 2)

    axes = axes.flat

    axes[0].plot(t, original_I, '.')
    axes[0].set_title('original I vs t')

    axes[1].plot(t, I, '.')
    axes[1].set_title('smoothed I vs t')

    axes[2].plot(t, original_dIdt, '.')
    axes[2].set_title('original dI/dt vs t')

    axes[3].plot(t, dIdt, '.')
    axes[3].set_title('smoothed dI/dt vs t')

    fig, axes = plt.subplots(1, 2)
    ax = axes[0]
    ax.plot(t, V, '.')
    ax.set_title('V vs t')
    ax = axes[1]
    ax.plot(t, V_R, '.')
    ax.set_title('V_R vs t')

    power = V_R * I
    E_vs_t = np.cumsum(power) * dt
    fig, axes = plt.subplots(2)
    ax: matplotlib.axes.Axes = axes[0]
    assert isinstance(fig, matplotlib.figure.Figure)
    ax.plot(t, power)
    ax.set_title('Power on wire vs time')
    ax.label_outer()
    ax.set_ylabel('Power [W]')

    ax: matplotlib.axes.Axes = axes[1]
    ax.plot(t, E_vs_t)
    ax.set_title('Energy vs time')
    ax.set_xlabel('time [s]')
    ax.set_ylabel('Energy [J]')

    fig.savefig(os.path.join(GRAPHS_FOLDER, 'single-wire-0.1mm.png'))

    E = np.sum(power) * dt
    print(f'energy={E}')

    C = 10.6e-6
    V_in = 27e3
    E_in = 1 / 2 * C * V_in ** 2
    print(f'E in={E_in}')
    print(f'efficiency={E / E_in}')



def calc_all_waveshocks_speed():
    print('---------single wire 0.6mm---------')
    start_waveshock1 = (122, 218)
    start_waveshock2 = (308, 503)
    end_waveshock2 = (483, 973)

    print(f'waveshock1 speed: {calc_waveshock_speed(start_waveshock2, start_waveshock1)}')
    print(f'waveshock2 speed: {calc_waveshock_speed(start_waveshock2, end_waveshock2)}')

    print()
    print('----------single wire 0.1mm--------')
    start_waveshock1 = (74, 161)
    end_waveshock1 = (417, 820)
    print(f'waveshock1 speed: {calc_waveshock_speed(end_waveshock1, start_waveshock1)}')


def calc_waveshock_speed(p1, p2):
    dt_per_pixel = 10000e-9 / 1392
    dy_per_pixel = 1000e-6 / 146

    dy = (p2[1] - p1[1]) * dy_per_pixel
    dt = (p2[0] - p1[0]) * dt_per_pixel

    return dy / dt


def main():
    # short_circuit()
    single_wire_600()
    single_wire_100()
    # calc_all_waveshocks_speed()


    plt.show()


if __name__ == '__main__':
    main()
